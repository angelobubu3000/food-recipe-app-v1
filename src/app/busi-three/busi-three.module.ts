import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BusiThreePageRoutingModule } from './busi-three-routing.module';

import { BusiThreePage } from './busi-three.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BusiThreePageRoutingModule
  ],
  declarations: [BusiThreePage]
})
export class BusiThreePageModule {}
