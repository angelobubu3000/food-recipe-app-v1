import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BusiThreePage } from './busi-three.page';

const routes: Routes = [
  {
    path: '',
    component: BusiThreePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BusiThreePageRoutingModule {}
