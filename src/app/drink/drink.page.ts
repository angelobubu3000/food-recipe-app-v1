import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-drink',
  templateUrl: './drink.page.html',
  styleUrls: ['./drink.page.scss'],
})
export class DrinkPage implements OnInit {

  drink: any[] = [
    { id: 1, title:'Bibingka', descrip: 'Its taste is usually slightly sweet, with a little bit of salty undertone for the contrast. Depending on the versions, this rice cake’s texture can range from being spongy to being sticky. It is also categorized as a “kakanin,” a collective term for foods made from rice or rice flour.',
    ingre: ' Main Ingredients: Galapong (Milled Glutinous Rice), Coconut Milk, Margarine, Sugar.' , 
    sub:'Bibingka is a food in the Philippines and is one of the many varieties of rice cakes in Filipino cuisine.',
    img: '/assets/images/main/bibingka.jpg'},

    { id: 2, title:'Biko', descrip: 'Biko is a type of sticky rice cake, otherwise known as kakanin. With a combination of coconut milk and brown sugar, biko is a delicious dessert or merienda to share with your loved ones! You can often find biko at birthday parties, fiestas, holiday parties, and family reunions, usually with other sticky rice treats.',
    ingre: ' Main Ingredients: Glutinous Rice, Coconut Milk, Brown Sugar.' , 
    sub:'Biko is a delicious treat that Filipinos all across the world enjoy.',
    img:'/assets/images/main/biko.jpg',},

    { id: 2, title:'Coffee Jelly', descrip: 'The Japanese people found new uses for the coffee plant, one of the best being coffee jelly. This fantastic combination of coffee and gelatin is served as dessert, or sometimes alongside liquid coffee. Individually, coffee and gelatin have wonderful health benefits. Combined together, coffee jelly is a nutritious snack or dessert.',
     ingre: ' Main Ingredients: Sweetened Sugar, Condense Milk, Instant Coffee, Table Cream, Unflavored Gelatin. ' , 
     sub:'Coffee Jelly was invented after westerners introduced coffee to the Japanese.',
    img:'/assets/images/main/coffee.jpg',},

    { id: 2, title:'Halo-Halo', descrip: 'While its Filipino name means ‘mix-mix’ in English, this favourite shaved ice dessert was introduced to the Filipinos by the Japanese settlers. According to historians, the Japanese had sweet desserts called ‘Mitsumame‘ or ‘mongo con hielo‘ and ‘kakigori’ which closely resemble the Filipinos’ Halo-halo.',
    ingre: ' Main Ingredients: Sweet red bean (munggo), Sweet white beans, Coconut gel (nata de coco), Macapuno (gelatinous coconut string), Fresh or canned jackfruit (cut into chunks), Shaved ice Evaporated milk Ube ice cream' , 
    sub:'This icy treat is known for its flavorful taste, and has a rich history that dates back to the pre-war Japanese period.',
    img:'/assets/images/main/halo.jpg',},

    { id: 2, title:'Leche Flan', descrip: 'It is a milk custard dessert made by baking a layer of caramel at the bottom of the custard mold. The custard is slowly baked in a water bath or steamed stove-top. After baking, the custard is chilled. Before serving, the mold is inverted and the caramel topping becomes a sauce to the flan.',
    ingre: ' Main Ingredients: Eggs, Condensed Milk, Sugar, Vanilla Extract.' , 
    sub:'Leche flan is simply a Filipino version of creme caramel.',
    img:'/assets/images/main/leche.jpg',},

    { id: 2, title:'Mais Con Yelo', descrip: 'This dessert is created by admixture whole kernel corn, milk, sugar and clean-shaven ice. Mais con yelo is one the simplest summer treats and is really refreshing.',
    ingre: ' Main Ingredients: Sweet Corn, Shaved Ice, Condensed Milk, Evaporated Milk, White Sugar, Keso.' , 
    sub:'Mais con Yelo or Maíz con hielo could be a creamy, sweet and refreshing dish common within the Philippines.',
    img:'/assets/images/main/mais.jpg',},

    { id: 2, title:'Pastillas', descrip: 'Pastillas is a milk-based confectionery with origins in the town of San Miguel in Bulacan, Philippines. From San Miguel, pastillas-making spread to other Philippine regions such as the provinces of Cagayan and Masbate.Initially, pastillas de leche were primarily home-made by carabao-rearing farmers.',
    ingre: ' Main Ingredients: Powder Milk, Condensed Milk, Sugar' , 
    sub:'Pastillas, also known as Pastillas de Leche or Pastiyema',
    img:'/assets/images/main/pastillas.jpg',},

    { id: 2, title:'Polvoron', descrip: 'This is considered as a dessert or snack in the Philippines wherein roasted rice puffs referred to locally as “pinipig” is added.There are several recipes for this Filipino treat that are available today. There are the cookies and cream, peanut, chocolate, and many more. ',
    ingre: ' Main Ingredients: Toasted Flour, Powdered Milk, Sugar, Butter' , 
    sub:'Polvoron is a semi-sweet concoction made of toasted flour, powdered milk, sugar, and butter.',
    img:'/assets/images/main/polvoron.jpg',},

    { id: 2, title:"Sago't Gulaman", descrip: 'This is Sago’t Gulaman. Sago, also known as tapioca pearls, is a starch extracted from tapioca which is a common food ingredient in Asian countries like China, Philippines, Malaysia and Thailand.',
    ingre: ' Main Ingredients: Sugar, Sago, Water, Tapioca  Pearls, Vanilla Extract, Grass Jelly Cubed.' , 
    sub:'Sago is a common ingredient in many desserts and beverages.',
    img:'/assets/images/main/sago.jpg',},

    { id: 2, title:"Yema", descrip: ' I don’t see the reason as to why not because egg yolk is a major ingredient for this sweet delicacy. If you like yema and enjoy eating it, you should know that this sweet treat is quick and easy to make.  You can make a batch in less than 30 minutes. This simple yema recipe might be able to help you earn extra money.',
    ingre: ' Main Ingredients: Egg Yolks, Condensed Milk, Butter, Walnuts, Peanuts' , 
    sub:'Yema is a type of Filipino candy named after the Spanish term for egg yolks.',
    img:'/assets/images/main/yema.jpg',},
  ];


// productId;

// foods: any = {};

//   constructor(private router: ActivatedRoute) {
// this.productId =this.router.snapshot.paramMap.get('id');
// this.router.queryParams.subscribe( res => {
//   console.log(res);
//   this.foods = res;
// })
//   }

  constructor(private router: Router) {}

goToDrinks(drink){

  this.router.navigate(['/drinks'],{
    queryParams: drink 
  })
}

  ngOnInit() {
    // With Routing in Ionic, The OnInit lifecycle hook 
    // may not get called consistently.
    console.log("CategoriesPage - OnInit")
  }
  
  ngOnDestroy() {
    // Likewise, this will may not consistently fire when you navigate away
    // from the component
    console.log("CategoriesPage - OnDestroy")
  }


}

