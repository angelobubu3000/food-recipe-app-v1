import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-drinks',
  templateUrl: './drinks.page.html',
  styleUrls: ['./drinks.page.scss'],
})
export class DrinksPage {

  productId;

drink: any = {};

  constructor(private router: ActivatedRoute) {
this.router.queryParams.subscribe( res => {
  console.log(res);
  this.drink = res;
})
  }}