import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MyenPageRoutingModule } from './myen-routing.module';

import { MyenPage } from './myen.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MyenPageRoutingModule
  ],
  declarations: [MyenPage]
})
export class MyenPageModule {}
