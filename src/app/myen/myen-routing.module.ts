import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MyenPage } from './myen.page';

const routes: Routes = [
  {
    path: '',
    component: MyenPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MyenPageRoutingModule {}
