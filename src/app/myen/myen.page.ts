import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-myen',
  templateUrl: './myen.page.html',
  styleUrls: ['./myen.page.scss'],
})
export class MyenPage  {

  adobo: any = {};

  constructor(private router: ActivatedRoute) {
  this.router.queryParams.subscribe( res => {
  console.log(res);
  this.adobo = res;
  })
  }
  }