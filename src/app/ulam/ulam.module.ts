import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UlamPageRoutingModule } from './ulam-routing.module';

import { UlamPage } from './ulam.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UlamPageRoutingModule
  ],
  declarations: [UlamPage]
})
export class UlamPageModule {}
