import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UlamPage } from './ulam.page';

const routes: Routes = [
  {
    path: '',
    component: UlamPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UlamPageRoutingModule {}
