import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BusiOnePage } from './busi-one.page';

const routes: Routes = [
  {
    path: '',
    component: BusiOnePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BusiOnePageRoutingModule {}
