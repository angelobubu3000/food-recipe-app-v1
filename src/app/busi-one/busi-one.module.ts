import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BusiOnePageRoutingModule } from './busi-one-routing.module';

import { BusiOnePage } from './busi-one.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BusiOnePageRoutingModule
  ],
  declarations: [BusiOnePage]
})
export class BusiOnePageModule {}
