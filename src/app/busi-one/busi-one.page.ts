import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-busi-one',
  templateUrl: './busi-one.page.html',
  styleUrls: ['./busi-one.page.scss'],
})
export class BusiOnePage implements OnInit {

  busi: any;
  constructor(private router: Router) {}
 ngOnInit(){
fetch('/assets/data/db.json').then(res => res.json()).then(json =>{
  console.log('busi::', json);
  this.busi = json;
});

 }
}

