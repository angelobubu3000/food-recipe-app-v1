import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';


const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'recipe',
    loadChildren: () => import('./recipe/recipe.module').then( m => m.RecipePageModule)
  },
  {
    path: 'categories',
    loadChildren: () => import('./categories/categories.module').then( m => m.CategoriesPageModule)
  },
  {
    path: 'detail',
    loadChildren: () => import('./detail/detail.module').then( m => m.DetailPageModule)
  },
  {
    path: 'meryen',
    loadChildren: () => import('./meryen/meryen.module').then( m => m.MeryenPageModule)
  },
  {
    path: 'ulam',
    loadChildren: () => import('./ulam/ulam.module').then( m => m.UlamPageModule)
  },
  {
    path: 'about',
    loadChildren: () => import('./about/about.module').then( m => m.AboutPageModule)
  },
  {
    path: 'contact',
    loadChildren: () => import('./contact/contact.module').then( m => m.ContactPageModule)
  },
  {
    path: 'side',
    loadChildren: () => import('./side/side.module').then( m => m.SidePageModule)
  },
  {
    path: 'sidedish',
    loadChildren: () => import('./sidedish/sidedish.module').then( m => m.SidedishPageModule)
  },
  {
    path: 'meryenda',
    loadChildren: () => import('./meryenda/meryenda.module').then( m => m.MeryendaPageModule)
  },
  {
    path: 'book',
    loadChildren: () => import('./book/book.module').then( m => m.BookPageModule)
  },
  {
    path: 'ulam-one',
    loadChildren: () => import('./ulam-one/ulam-one.module').then( m => m.UlamOnePageModule)
  },
  {
    path: 'ulam-two',
    loadChildren: () => import('./ulam-two/ulam-two.module').then( m => m.UlamTwoPageModule)
  },
  {
    path: 'business',
    loadChildren: () => import('./business/business.module').then( m => m.BusinessPageModule)
  },
  {
    path: 'drink',
    loadChildren: () => import('./drink/drink.module').then( m => m.DrinkPageModule)
  },
  {
    path: 'drinks',
    loadChildren: () => import('./drinks/drinks.module').then( m => m.DrinksPageModule)
  },
  {
    path: 'book-sdish',
    loadChildren: () => import('./book-sdish/book-sdish.module').then( m => m.BookSdishPageModule)
  },
  {
    path: 'side-one',
    loadChildren: () => import('./side-one/side-one.module').then( m => m.SideOnePageModule)
  },
  {
    path: 'book-myen',
    loadChildren: () => import('./book-myen/book-myen.module').then( m => m.BookMyenPageModule)
  },
  {
    path: 'myen',
    loadChildren: () => import('./myen/myen.module').then( m => m.MyenPageModule)
  },
  {
    path: 'book-drinks',
    loadChildren: () => import('./book-drinks/book-drinks.module').then( m => m.BookDrinksPageModule)
  },
  {
    path: 'drink-one',
    loadChildren: () => import('./drink-one/drink-one.module').then( m => m.DrinkOnePageModule)
  },
  {
    path: 'busi-one',
    loadChildren: () => import('./busi-one/busi-one.module').then( m => m.BusiOnePageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'busi-two',
    loadChildren: () => import('./busi-two/busi-two.module').then( m => m.BusiTwoPageModule)
  },
  {
    path: 'busi-three',
    loadChildren: () => import('./busi-three/busi-three.module').then( m => m.BusiThreePageModule)
  },
  {
    path: 'busi-four',
    loadChildren: () => import('./busi-four/busi-four.module').then( m => m.BusiFourPageModule)
  },
  {
    path: 'busi-five',
    loadChildren: () => import('./busi-five/busi-five.module').then( m => m.BusiFivePageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
