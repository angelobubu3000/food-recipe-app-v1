import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SideOnePageRoutingModule } from './side-one-routing.module';

import { SideOnePage } from './side-one.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SideOnePageRoutingModule
  ],
  declarations: [SideOnePage]
})
export class SideOnePageModule {}
