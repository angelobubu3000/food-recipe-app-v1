import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SideOnePage } from './side-one.page';

const routes: Routes = [
  {
    path: '',
    component: SideOnePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SideOnePageRoutingModule {}
