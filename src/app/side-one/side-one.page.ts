import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-side-one',
  templateUrl: './side-one.page.html',
  styleUrls: ['./side-one.page.scss'],
})
export class SideOnePage {

  adobo: any = {};

  constructor(private router: ActivatedRoute) {
  this.router.queryParams.subscribe( res => {
  console.log(res);
  this.adobo = res;
  })
  }
  }