import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MeryendaPageRoutingModule } from './meryenda-routing.module';

import { MeryendaPage } from './meryenda.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MeryendaPageRoutingModule
  ],
  declarations: [MeryendaPage]
})
export class MeryendaPageModule {}
