import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MeryendaPage } from './meryenda.page';

const routes: Routes = [
  {
    path: '',
    component: MeryendaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MeryendaPageRoutingModule {}
