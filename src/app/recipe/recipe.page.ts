import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-recipe',
  templateUrl: './recipe.page.html',
  styleUrls: ['./recipe.page.scss'],
})
export class RecipePage {

  constructor(private router: Router) {}

  goToDrink(){
    this.router.navigateByUrl('/book-drinks');
}
  goToMeryen(){
    this.router.navigateByUrl('/book-myen');
}
  goToSide(){
    this.router.navigateByUrl('/book-sdish');
}
  goToBook(){
    this.router.navigateByUrl('/book');
}
  goToCategories(){
    this.router.navigateByUrl('/categories');
}
  goToRecipe(){
    this.router.navigateByUrl('/recipe');
}
  goToHome(){
  this.router.navigateByUrl('/home');
}


  foods: any[] = [
    { title:'Pinoy Ulam', descrip: 'Adobo, Sinigang, Tinola, Menudo, Caldereta....'},
    { title:'Meryenda', descrip: 'Street Foods, Turon, Banana Que....'},
    { title:'Other Delecacies', descrip: 'Graham Balls etc etc....'},
  ];
}
