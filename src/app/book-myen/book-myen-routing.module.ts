import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BookMyenPage } from './book-myen.page';

const routes: Routes = [
  {
    path: '',
    component: BookMyenPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BookMyenPageRoutingModule {}
