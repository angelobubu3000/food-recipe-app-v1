import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BookMyenPageRoutingModule } from './book-myen-routing.module';

import { BookMyenPage } from './book-myen.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BookMyenPageRoutingModule
  ],
  declarations: [BookMyenPage]
})
export class BookMyenPageModule {}
