import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UlamTwoPageRoutingModule } from './ulam-two-routing.module';

import { UlamTwoPage } from './ulam-two.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UlamTwoPageRoutingModule
  ],
  declarations: [UlamTwoPage]
})
export class UlamTwoPageModule {}
