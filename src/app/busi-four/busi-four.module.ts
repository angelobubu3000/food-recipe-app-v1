import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BusiFourPageRoutingModule } from './busi-four-routing.module';

import { BusiFourPage } from './busi-four.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BusiFourPageRoutingModule
  ],
  declarations: [BusiFourPage]
})
export class BusiFourPageModule {}
