import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BusiFourPage } from './busi-four.page';

const routes: Routes = [
  {
    path: '',
    component: BusiFourPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BusiFourPageRoutingModule {}
