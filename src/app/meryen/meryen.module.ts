import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MeryenPageRoutingModule } from './meryen-routing.module';

import { MeryenPage } from './meryen.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MeryenPageRoutingModule
  ],
  declarations: [MeryenPage]
})
export class MeryenPageModule {}
