import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MeryenPage } from './meryen.page';

const routes: Routes = [
  {
    path: '',
    component: MeryenPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MeryenPageRoutingModule {}
