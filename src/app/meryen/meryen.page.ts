import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-meryen',
  templateUrl: './meryen.page.html',
  styleUrls: ['./meryen.page.scss'],
})
export class MeryenPage{


  mermer: any[] = [
    { id: 1, title:'Arroz Caldo', descrip: 'It is a rice porridge casserole rice and chicken gruel heavily infused with ginger and garnished with toasted garlic, scallions, and black pepper. It is usually served with calamansi or fish sauce as condiments, as well as a hard-boiled egg. Perfect for a midday snack or light meal. Selling arroz caldo is a great home-based food business idea since it’s very easy to prepare and can be done at the comfort of your home. ',
    ingre: ' Main Ingredients: Chicken, Rice, Garlic, Onion, Ground Meat, Ginger, Black Pepper, Boiled Egg.' , 
    sub:'Arroz Caldo is the ultimate comfort food of the Filipinos.',
    img: '/assets/images/main/arroz.jpg'},

    { id: 2, title:'Banana Que', descrip: 'A banana cue is simply made from cardaba or saba bananas, a kind of banana that grows in many tropical Asian countries like the Philippines, rolled in a brown sugar and then deep fried. It is usually skewered on a bamboo stick and is sold on the streets.',
    ingre: ' Main Ingredients: Banana Saba, Brown Sugar, Cooking Oil' , 
    sub:'Banana cue  is a popular snack in the Philippines. This is mostly eaten as an afternoon snack and is sometimes served as dessert.',
    img:'/assets/images/main/bananaq.jpg',},

    { id: 2, title:'Chicken Meat Balls', descrip: 'This chicken meat ball have less cholesterol provided that you will use chicken breast instead of other parts of the chicken that contains too much fat. Another healthy ingredient added to this meat balls is the oatmeal as an extender and to create a better texture. This food is usual meryenda of filipinos  that we can see in the streets because this recipe is easy to make and budget friendly.',
     ingre: ' Main Ingredients: Place ground chicken, Eggs, Breadcrumbs, Parmesan cheese, Olive oil' , 
     sub:'Chicken meat balls is another variation of the usual pork meat balls we used to love.',
    img:'/assets/images/main/chicken.jpg',},

    { id: 2, title:'Empanada', descrip: 'An empanada is a type of baked or fried turnover consisting of pastry and filling. They are made by folding dough over a filling, which may consist of meat, cheese, tomato, corn, or other ingredients, and then cooking the resulting turnover, either by baking or frying.',
    ingre: ' Main Ingredients: Cheese, Flour, Onion, Egg, Beef, Ground Meat, Salt, Ground Beef Bell Pepper, Empanada Dough.' , 
    sub:'The name comes from the Galician verb empanar, and translates as "enbreaded", that is, wrapped or coated in bread.  ',
    img:'/assets/images/main/empanada.jpg',},

    { id: 2, title:'Kwek-Kwek', descrip: 'While tokneneng is made with boiled chicken or duck eggs, kwek kwek is the smaller sized variety, which usually employs tiny quail eggs. Both types are distinguished by the deep orange color, provided by an unusual batter in which eggs are coated before frying. The thick batter is made with flour, cornstarch, and water, with the addition of annatto powder, a unique ingredient which gives the dish its remarkable orange color. Hard-boiled quail eggs are thoroughly coated in batter, and fried until crispy.',
    ingre: ' Main Ingredients: Quail Eggs, Egg, Flour, Salt, Water, Seasonings.' , 
    sub:'kwek is a type of tokneneng, the omnipresent Filippino street food..',
    img:'/assets/images/main/kwek.jpg',},

    { id: 2, title:'Lumpiang  Sariwa', descrip: 'It literally translates to  Fresh Spring Rolls. The filling is composed of various vegetables, some shrimps, and meat. Firm Tofu may also be added. The filling is then wrapped or rolled in a crepe-like wrapper. We call it fresh because you do not fry it as we do the other spring rolls, like Lumpiang Shanghai and Lumpiang Prito or Togue. It also has a special sauce that is sweet and savory and then topped with chopped peanuts.',
    ingre: ' Main Ingredients: Wrapper, Acai, Jucara  Pejibayes, Vegetables.' , 
    sub:'Lumpiang Sariwa is another variety of Filipino spring rolls.',
    img:'/assets/images/main/lumpiang-sariwa.jpg',},

    { id: 2, title:'Maruya', descrip: 'Maruya is regarded either as a snack or a light meal. It is often served by street vendors and food sellers at outdoor events. Once they are cooked, the fried banana pieces are sweetened by rolling them in sugar.',
    ingre: ' Main Ingredients: Banana, Fresh milk, Egg, baking powder, All purpose flour' , 
    sub:'Maruya originates from the Philippines.',
    img:'/assets/images/main/maruya.jpg',},

    { id: 2, title:'Pancit', descrip: 'Basically any and all localized noodles typically sautéed with meat, seafood, and vegetables. Pancit comes from the Hokkien word, “pian i sit”, meaning “something conveniently cooked fast”. Essentially, the word referred to convenience food. In this case in the Philippines, it referred to noodles cooked with meat and vegetables as a complete meal on one plate.',
    ingre: ' Main Ingredients: Noodles, Chicken, Pork, Vegetables, Soy Sauce, Oyster Sauce, Garlic, Salt.' , 
    sub:'The name “pancit” means “noodles”, which can either be rice noodles, egg noodles, or mung bean noodles.',
    img:'/assets/images/main/pancit.jpg',},

    { id: 2, title:'Siomai', descrip: 'This popular dumpling has made its way to the heart of the Filipino’s as evidenced by the hundreds of stalls, eateries, and restaurants who serve them. Traditionally cooked through steaming, siomai nowadays are also served fried complemented with soy sauce and calamansi.',
    ingre: ' Main Ingredients: Shrimp, Ground pork, Jicama, Pork seasoning, Sesame Oil, Soy Sauce, Chili, Garlic' , 
    sub:'Pork Siomai is a traditional Chinese dumpling.',
    img:'/assets/images/main/siomai.jpg',},

    { id: 2, title:"Turon", descrip: 'A popular Filipino snack consisting of a banana wrapped in a deep-fried spring roll wrapper, usually coated with caramelized sugar. Slices of jackfruit often accompany the banana as a complementary filling. It is one of the more common street foods in the Philippines, sold by many street vendors in both urban and rural areas. Turon is often sold alongside other banana-based snack foods, such as banana cue and maruya, or banana fritter.',
    ingre: ' Main Ingredients: Banana Saba, Lumpia Wrapper, Sugar, Cooking Oil' , 
    sub:'Turon, sometimes referred to as banana lumpia.',
    img:'/assets/images/main/turon.jpg',},
  ];



  constructor(private router: Router) {}

  goToMermer(mermer){

    this.router.navigate(['/meryenda'],{
      queryParams: mermer 
    })
  }}
