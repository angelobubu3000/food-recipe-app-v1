import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BusiTwoPageRoutingModule } from './busi-two-routing.module';

import { BusiTwoPage } from './busi-two.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BusiTwoPageRoutingModule
  ],
  declarations: [BusiTwoPage]
})
export class BusiTwoPageModule {}
