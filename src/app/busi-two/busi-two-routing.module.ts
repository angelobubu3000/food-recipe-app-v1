import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BusiTwoPage } from './busi-two.page';

const routes: Routes = [
  {
    path: '',
    component: BusiTwoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BusiTwoPageRoutingModule {}
