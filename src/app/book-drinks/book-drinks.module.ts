import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BookDrinksPageRoutingModule } from './book-drinks-routing.module';

import { BookDrinksPage } from './book-drinks.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BookDrinksPageRoutingModule
  ],
  declarations: [BookDrinksPage]
})
export class BookDrinksPageModule {}
