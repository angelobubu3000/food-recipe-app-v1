import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-book-drinks',
  templateUrl: './book-drinks.page.html',
  styleUrls: ['./book-drinks.page.scss'],
})
export class BookDrinksPage implements OnInit {

  meryah: any[]= [

    {  title: 'Bibingka', 
    main: 'Ingredients: ', 
    ingre: "• 1 cup rice flour",
    ingree: "• 1/8 teaspoon salt",
    ingree1: "• 2 1/2 teaspoon baking powder",
    ingree2: '• 3 tablespoons butter',
    ingree3: '• 1 cup granulated sugar',
    ingree4:	'• 1 cup coconut milk',
    ingree5:	'• 1/4 cup fresh milk ', 
    ingree7:	"• 1 piece salted duck egg sliced",
    ingree8:	"• 1/2 cup grated cheese",
    ingree9:	"• 3 pieces raw eggs",
    ingree10:	"• Pre-cut banana leaf",
    prod:	"Procedure:",
    proc: '1. Preheat oven to 375 degrees Fahrenheit',
    proc1:	'2.	 Combine rice flour, baking powder, and salt then mix well. Set aside.',
    proc2:	'3.	Cream butter then gradually put-in sugar while whisking.', 
    proc3:	"4.	Add the eggs then whisk until every ingredient is well incorporated.",
    proc4:	"5.	Gradually add the rice flour, salt, and baking powder mixture then continue mixing.",
    proc5:	"6.	Pour-in coconut milk and fresh milk then whisk some more for 1 to 2 minutes.",
    proc7:	"7.	Arrange the pre-cut banana leaf on a cake pan or baking pan.",
    proc8:	"8.	Pour the mixture on the pan.",
    proc9:	"9.	Bake for 15 minutes.",
    proc10:	"10. Remove from the oven then top with sliced salted egg and grated cheese (do not turn the oven off).	",
    proc11:	"11.	Put back in the oven and bake for 15 to 20 minutes or until the color of the top turn medium brown.",
    proc12:	"12.	Remove from the oven and let cool.",
    proc13:	"13.	Brush with butter and sprinkle some sugar on top. You can also top this with grated coconut.",
    proc14:	"14.	Serve. Share and enjoy!",
    img: '/assets/images/main/bibingka.jpg', },

    {  title: 'Biko', 
    main: 'Ingredients: ', 
    ingre: "• 2 cups glutinous rice aka sticky rice or malagkit",
    ingree: "• 1 1/2 cups water",
    ingree1: "• 2 cups brown sugar",
    ingree2: '• 4 cups coconut milk',
    ingree3: '• 1/2 tsp salt',
    prod:	"Procedure:",
    proc: '1. Combine the sticky rice and water in a rice cooker and cook until the rice is ready (we intentionally combined lesser amount of water than the usual so that the rice will not be fully cooked)',
    proc1:	'2.	While the rice is cooking, combine the coconut milk with brown sugar and salt in a separate pot and cook in low heat until the texture becomes thick. Stir constantly. ',
    proc2:	'3.	Once the rice is cooked and the coconut milk-sugar mixture is thick enough, add the cooked rice in the coconut milk and sugar mixture then mix well. Continue cooking until all the liquid evaporates (but do not overcook).', 
    proc3:	"4.	Scoop the cooked biko and place it in a serving plate then flatten the surface.",
    proc4:	"5.	Share and Enjoy!",
    img: '/assets/images/main/biko.jpg', },

    {  title: 'Coffee Jelly', 
    main: 'Ingredients: ', 
    ingre: "• 1 box (1 ounce) unflavored gelatin (Use Knox)",
    ingree: "• 4 cups water",
    ingree1: "• 2 tablespoons instant coffee",
    ingree2: '• 1/2 cup sugar',
    ingree3: '• 1 can (14 ounces) condensed milk',
    ingree4:	'• 1 can (12.8 ounces) table cream',
    prod:	"Procedure:",
    proc: '1. In a sauce pot, bring 3 cups of the water to a boil. Add instant coffee and sugar. Stir until dissolved.',
    proc1:	'2.	 In a large bowl, sprinkle the gelatin on the remaining 1 cup of cold water. Let stand for about 1 minute or until gelatin powder begins to bloom.',
    proc2:	'3.	Gradually add the 3 cups of boiling coffee and stir constantly for about 2 to 3 minutes or until gelatin is completely dissolved and no granules are visible.', 
    proc3:	"4.	Transfer mixture into a  baking dish and allow to completely cool. Refrigerate for about 2 to 3 hours or until completely set.",
    proc4:	"5.	In a bowl, combine condensed milk and table cream. Stir until blended.",
    proc5:	"6.	Cut set gelatin into 1-inch cubes and divide into serving cups. Top with sweetened cream. Garnish with whipped cream, if desired. Serve cold.",
    img: '/assets/images/main/coffee.jpg', },

    {  title: 'Halo-Halo', 
    main: 'Ingredients: ', 
    ingre: "• sweet red bean (munggo)",
    ingree: "• sweet white beans ",
    ingree1: "• coconut gel (nata de coco)",
    ingree2: '• macapuno (gelatinous coconut string)',
    ingree3: '• Fresh or canned jackfruit, cut into chunks',
    ingree4:	'• Shaved ice',
    ingree5:	'•  Evaporated milk', 
    ingree7:	"• Ube ice cream",
    prod:	"Procedure:",
    proc: '1. In a serving glass, layer sweet red bean, sweet white beans, coconut gel, macapuno, and jackfruit. Top with shaved ice. Drizzle about one tablespoon of evaporated milk over shaved ice. Top with a scoop of ube ice cream. Enjoy immediately!',
    img: '/assets/images/main/halo.jpg', },

    {  title: 'Leche Flan', 
    main: 'Ingredients: ', 
    ingre: "• 10 pieces egg",
    ingree: "• 1 can condensed milk (14 oz)",
    ingree1: "• 1 cup fresh milk or evaporated milk",
    ingree2: '• 1 cup granulated sugar',
    ingree3: '• 1 teaspoon vanilla extract',
    prod:	"Procedure:",
    proc: '1. Using all the eggs, separate the yolk from the egg white (only egg yolks will be used).',
    proc1:	'2.	Place the egg yolks in a big bowl then beat them using a fork or an egg beater ',
    proc2:	'3.	Add the condensed milk and mix thoroughly', 
    proc3:	"4.	Pour-in the fresh milk and Vanilla. Mix well",
    proc4:	"5.	Put the mold (llanera) on top of the stove and heat using low fire",
    proc5:	"6.	Put-in the granulated sugar on the mold and mix thoroughly until the solid sugar turns into liquid (caramel) having a light brown color. Note: Sometimes it is hard to find a Llanera (Traditional flan mold) depending on your location. I find it more convenient to use individual Round Pans in making leche flan.",
    proc7:	"7.	Spread the caramel (liquid sugar) evenly on the flat side of the mold",
    proc8:	"8.	Wait for 5 minutes then pour the egg yolk and milk mixture on the mold",
    proc9:	"9.	Cover the top of the mold using an Aluminum foil",
    proc10:	"10.	Steam the mold with egg and milk mixture for 30 to 35 minutes.",
    proc11:	"11.	After steaming, let the temperature cool down then refrigerate",
    proc12:	"12.	Serve for dessert. Share and Enjoy!",
    img: '/assets/images/main/leche.jpg', },

    {  title: 'Mais Con Yelo', 
    main: 'Ingredients: ', 
    ingre: "• Sweet Corn (canned or Frozen)",
    ingree: "• Shaved Ice",
    ingree1: "• Sweetened Condensed Milk",
    ingree2: '• Evaporated Milk',
    ingree3: '• White Sugar',
    ingree4:	'• Keso (Optional)',
    ingree5:	'•  Saging na Saba (Optional)', 
    ingree7:	"• Corn Flakes (Optional)",
    prod:	"Procedure:",
    proc: '1. Layer the Sweet Corn and the Shaved Ice in a Glass',
    proc1:	'2.	 Add Milk and Sugar, Condensed Milk, Evaporated Milk (adjust according to taste) and stir with a spoon.',
    proc2:	'3.	Add other ingredients like Keso, Saging na Saba or CornFlakes.', 
    proc3:	"4.	Serve immediately.",
    img: '/assets/images/main/mais.jpg', },

    {  title: 'Pastillas', 
    main: 'Ingredients: ', 
    ingre: "• 2 cups powdered milk sifted",
    ingree: "• 1 can 14 ounces condensed milk",
    ingree1: "• ½ cup granulated sugar sifted",
    prod:	"Procedure:",
    proc: '1. Place the condensed milk in a large mixing bowl',
    proc1:	'2.	 Gradually fold-in the powdered milk. The texture of the mixture will be similar to dough once all the powdered milk is completely added.',
    proc2:	'3.	Scoop some of the mixture and mold into cylinders.', 
    proc3:	"4.	Roll each molded cylindrical mixture on granulated sugar",
    proc4:	"5.	Wrap in paper or cellophane.",
    proc5:	"6.	Serve for dessert. Share and enjoy!",
    img: '/assets/images/main/pastillas.jpg', },

    {  title: 'Polvoron', 
    main: 'Ingredients: ', 
    ingre: "• 4 cups all purpose flour",
    ingree: "• 2 cups powdered milk",
    ingree1: "• 3/4 cup pinipig crushed",
    ingree2: '• 1 1/8 cup butter softened',
    ingree3: '• 1 1/2 cup granulated sugar',
    prod:	"Procedure:",
    proc: '1. Heat a frying pan or a wok then put-in the flour. Toast the flour until you smell the aroma and the color turns light brown. Make sure to stir while toasting to prevent the flour from being burnt.',
    proc1:	'2.	 Once the flour is toasted, let it cool down for about 20 minutes then transfer to a mixing bowl.',
    proc2:	'3.	Add the powdered milk then stir using a balloon whisk.', 
    proc3:	"4.	Put-in the granulated sugar then stir again",
    proc4:	"5.	Add-in the crushed pinipig then stir until very ingredient is well distributed.",
    proc5:	"6.	Pour-in the softened butter and mix well. You may use your clean hands in doing the procedure. After mixing, let it stand for at least 10 minutes to allow the butter to cool. This will make the mixture more intact.",
    proc7:	"7.	Using a polvoron molder, scoop the mixture and put it on top of a pre-cut Japanese paper or cellophane then wrap.",
    proc8:	"8.	Serve. Share and enjoy!",
    img: '/assets/images/main/polvoron.jpg', },

    {  title: "Sago't Gulaman", 
    main: 'Ingredients: ', 
    ingre: "• 3 cups of brown sugar for syrup",
    ingree: "• 1 cup of brown sugar for sago",
    ingree1: "• 3 cups of water",
    ingree2: '• 3 cups of tapioca pearls',
    ingree3: '• 2 tablespoons of vanilla extract',
    ingree4:	'• 1 can of grass jelly, cubed',
    ingree5:	'•  Ice', 
    prod:	"Procedure:",
    proc: '1. Soak the tapioca pearls in water for at least an hour to soften them.',
    proc1:	'2.	 Drain it then place them in a large pot with 3 cups of the water and 1 cup of brown sugar.',
    proc2:	'3.	Bring the tapioca pearls to a boil.', 
    proc3:	"4.	Cook until the tapioca pearls become transparent then drain them and set them aside to cool.",
    proc4:	"5.	In a separate pot add the 2 tbsp of sugar and scorch it, but do not burn it.",
    proc5:	"6.	Then add the water, vanilla and the remaining sugar.",
    proc7:	"7.	Bring it to a boil then reduce heat and simmer for 5 minutes.",
    proc8:	"8.	Make sure that the sugar is totally dissolved, then you have a simple syrup",
    proc9:	"9.	Strain the syrup using a cheese cloth or any clean cloth to separate the sand-like grains from the sugar, leaving a clear brown syrup.",
    proc10:	"10.	Now pour the syrup in glass then add tapioca pearls, cubed grass jelly, water and ice.",
    img: '/assets/images/main/sago.jpg', },

    {  title: 'Yema', 
    main: 'Ingredients: ', 
    ingre: "• 7 pieces egg yolks",
    ingree: "• 1 can condensed milk (14 oz.)",
    ingree1: "• 3 tablespoons butter",
    ingree2: '• 5 tablespoons chopped walnuts or peanuts',
    prod:	"Procedure:",
    proc: '1. Combine egg yolks and condensed milk. Mix well.',
    proc1:	'2.	 Add chopped walnuts. Whisk until well blended.',
    proc2:	'3.	Melt butter in a cooking pot or pan. Pour the egg yolk mistuxe into the pan.', 
    proc3:	"4.Continue to cook until the consistency becomes very thick to the point that it can easily form a shape when molded.	",
    proc4:	"5.	Transfer the mixture to a bowl. Let it cool down.",
    proc5:	"6.	Cut the cellophane into 3x3 inch pieces. Scoop 1 tablespoons of mixture and place at the middle of the cellophane. Wrap the yema while molding to form a pyramid shape piece (note: watch the video for details). Continue to wrap the yema until all the mixture are consumed.",
    proc7:	"7.	Serve . Share and enjoy.",
    img: '/assets/images/main/yema.jpg', },
    ];



  constructor(private router: Router) {}


  goToMyen(meryah){

    this.router.navigate(['/drink-one'],{
      queryParams: meryah
    })
  }
  
    ngOnInit() {
      // With Routing in Ionic, The OnInit lifecycle hook 
      // may not get called consistently.
      console.log("CategoriesPage - OnInit")
    }
    
    ngOnDestroy() {
      // Likewise, this will may not consistently fire when you navigate away
      // from the component
      console.log("CategoriesPage - OnDestroy")
    }
  
  
}