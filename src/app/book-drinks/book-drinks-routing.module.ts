import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BookDrinksPage } from './book-drinks.page';

const routes: Routes = [
  {
    path: '',
    component: BookDrinksPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BookDrinksPageRoutingModule {}
