import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UlamOnePageRoutingModule } from './ulam-one-routing.module';

import { UlamOnePage } from './ulam-one.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UlamOnePageRoutingModule
  ],
  declarations: [UlamOnePage]
})
export class UlamOnePageModule {}
