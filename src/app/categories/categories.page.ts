import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.page.html',
  styleUrls: ['./categories.page.scss'],
})
export class CategoriesPage implements OnInit, OnDestroy {

  foods: any[] = [
    { id: 1, title:'Pinoy Ulam', descrip: 'Adobo, Sinigang, Tinola, Menudo, Caldereta....'},
    { id: 2, title:'Meryenda', descrip: 'Street Foods, Turon, Banana Que....'},
    { id: 3, title:'Other Delecacies', descrip: 'Graham Balls etc etc....'},
  ];

  // foodss: any[] = [
  //   { id: 1, title:'Adobo', descrip: 'Masarap pag luto'},
  //   { id: 2, title:'Sinigang', descrip: 'Sinigang Sinigang Sinigang'},
  //   { id: 3, title:'Tinola', descrip: 'Tinolang Walang sabaw'},
  // ];

  constructor(private router: Router) {}

  goToDetail(foods){

    this.router.navigate(['/detail'],{
      queryParams: foods 
    })
}

goToDrink(){
  this.router.navigateByUrl('/drink');
}

goToSide(){
  this.router.navigateByUrl('/side');
}

goToMeryen(){
  this.router.navigateByUrl('/meryen');
}
  goToCategories(){
    this.router.navigateByUrl('/categories');
}
  goToRecipe(){
    this.router.navigateByUrl('/recipe');
}
  goToHome(){
  this.router.navigateByUrl('/home');
}

 


ngOnInit() {
  // With Routing in Ionic, The OnInit lifecycle hook 
  // may not get called consistently.
  console.log("CategoriesPage - OnInit")
}

ngOnDestroy() {
  // Likewise, this will may not consistently fire when you navigate away
  // from the component
  console.log("CategoriesPage - OnDestroy")
}

ionViewWillEnter() {
  // This method will be called every time the component is navigated to
  // On initialization, both ngOnInit and this method will be called

  console.log("CategoriesPage - ViewWillEnter")
}

ionViewWillLeave() {
  // This method will be called every time the component is navigated away from
  // It would be a good method to call cleanup code such as unsubscribing from observables

  console.log("CategoriesPage - ViewWillLeave")
}

}