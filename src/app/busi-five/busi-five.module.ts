import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BusiFivePageRoutingModule } from './busi-five-routing.module';

import { BusiFivePage } from './busi-five.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BusiFivePageRoutingModule
  ],
  declarations: [BusiFivePage]
})
export class BusiFivePageModule {}
