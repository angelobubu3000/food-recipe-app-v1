import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BusiFivePage } from './busi-five.page';

const routes: Routes = [
  {
    path: '',
    component: BusiFivePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BusiFivePageRoutingModule {}
