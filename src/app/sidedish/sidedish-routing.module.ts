import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SidedishPage } from './sidedish.page';

const routes: Routes = [
  {
    path: '',
    component: SidedishPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SidedishPageRoutingModule {}
