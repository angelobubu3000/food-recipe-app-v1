import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SidedishPageRoutingModule } from './sidedish-routing.module';

import { SidedishPage } from './sidedish.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SidedishPageRoutingModule
  ],
  declarations: [SidedishPage]
})
export class SidedishPageModule {}
