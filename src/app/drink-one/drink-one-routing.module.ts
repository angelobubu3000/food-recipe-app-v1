import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DrinkOnePage } from './drink-one.page';

const routes: Routes = [
  {
    path: '',
    component: DrinkOnePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DrinkOnePageRoutingModule {}
