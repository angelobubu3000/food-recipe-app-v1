import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DrinkOnePageRoutingModule } from './drink-one-routing.module';

import { DrinkOnePage } from './drink-one.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DrinkOnePageRoutingModule
  ],
  declarations: [DrinkOnePage]
})
export class DrinkOnePageModule {}
