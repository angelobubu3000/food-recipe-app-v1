import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-drink-one',
  templateUrl: './drink-one.page.html',
  styleUrls: ['./drink-one.page.scss'],
})
export class DrinkOnePage  {

  adobo: any = {};

  constructor(private router: ActivatedRoute) {
  this.router.queryParams.subscribe( res => {
  console.log(res);
  this.adobo = res;
  })
  }
  }
