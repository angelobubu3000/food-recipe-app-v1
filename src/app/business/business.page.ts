import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-business',
  templateUrl: './business.page.html',
  styleUrls: ['./business.page.scss'],
})
export class BusinessPage{

  busi: any[]= [

  {  title: 'Business Plan', 
  par1: 'Paragraph 1', 
  par2: "Paragraph 2 ",
  },
  {  title: 'Business Plan', 
  par1: 'Paragraph 1', 
  par2: "Paragraph 2 ",
  },
  {  title: 'Business Plan', 
  par1: 'Paragraph 1', 
  par2: "Paragraph 2 ",
  },
  {  title: 'Business Plan', 
  par1: 'Paragraph 1', 
  par2: "Paragraph 2 ",
  },
  {  title: 'Business Plan', 
  par1: 'Paragraph 1', 
  par2: "Paragraph 2 ",
  },
  ]


  constructor(private router: Router) {}




  goToBusi(busi){

    this.router.navigate(['/busi-one'],{
      queryParams: busi
    })
}

goToDrink(){
  this.router.navigateByUrl('/drink');
}

goToSide(){
  this.router.navigateByUrl('/side');
}

goToMeryen(){
  this.router.navigateByUrl('/meryen');
}
  goToCategories(){
    this.router.navigateByUrl('/categories');
}
  goToRecipe(){
    this.router.navigateByUrl('/recipe');
}
  goToHome(){
  this.router.navigateByUrl('/home');
}

}
