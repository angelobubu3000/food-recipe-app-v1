import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit, OnDestroy   {

  ulams: any[] = [
    
    { id: 1, title:'Adobo', descrip: 'Adobo is created by using a marinade wherein it consists of pantry basics such as white vinegar, soy sauce, garlic, peppercorns, and bay leaves. In Spanish cooking, the common practice of marinating meat is made from vinegar, salt, garlic, paprika, and oregano. Furthermore, the name “adobo” was given by the Spanish colonists to the cooking method indigenous to the Philippines since their marinades were familiar.     ',
    ingre: ' Main Ingredients: Vinegar, Soy sauce, Garlic, Bay leaves, and Black pepper, Meat/Chicken.' , 
    sub:'Adobo is derived from the Spanish word adobar, which means “marinade” or “pickling sauce.',
    img: '/assets/images/main/Adobo.jpg'},

    { id: 2, title:'Afritada', descrip: 'The origin of this Filipino dish dates back to the colonial era (1521-1898) of Spain in the Philippines. One of the methods of cooking that was introduced by the Spaniards to Filipinos is pan-frying, in which a minimal use of cooking oil is used just to lubricate the pan for frying. In cooking Chicken Afritada, therefore, is to first pan fry the chicken and other vegetables before stewing it in tomato sauce.',
    ingre: ' Main Ingredients: Chicken/Beef/Pork, Tomato sauce, Carrots, Potatoes, Red and Green Bell Peppers.' , 
    sub:'Afritada was derived from the Spanish word “fritada” which means “fried”',
    img:'/assets/images/main/Afritada.jpg',},

    { id: 2, title:'Buffalo Wings', descrip: 'There are several different claims about the invention of Buffalo wings. One of the claims is that Buffalo wings were first prepared at the Anchor Bar in Buffalo, New York, by Teressa Bellissimo, who owned the bar with husband Frank in 1964.',
     ingre: ' Main Ingredients: Chicken Wing, Cayenne Pepper, Hot Sauce, Butter.' , 
     sub:'The name "Buffalo" is now also applied to other spiced fried foods served with dipping sauces.',
    img:'/assets/images/main/buffalo.jpg',},

    { id: 2, title:'Bulalo', descrip: 'Bulalo is native to the Southern Luzon region of the Philippines. Moreover, many references have evolved regarding its origin. One of them says that this type of dish originated from Batangas where you find the many versions of Bulalo. Another reference says that it came from Tagaytay in which there are a lot of cows.',
    ingre: ' Main Ingredients: Beef, Cabbage, Chinese Cabbage, Corn, Fish Sauce, Garlic, Onion, Potatoes, Lemon.' , 
    sub:'It is popularly known for having the beef shank and bone marrow and letting its flavors melt into a super tasty broth.',
    img:'/assets/images/main/Bulalo.jpg',},

    { id: 2, title:'Caldereta', descrip: 'Caldereta is also a name for a Spanish stew, although seafood such as the red lobster is primarily used instead of goat or beef. The history behind this stew is uncertain, but what is evident is the dish’s Spanish roots, since the Philippines were under Spanish rule for more than 300 years.',
    ingre: ' Main Ingredients: Goat / Mutton Shoulders, Corn Oil, Onion, Garlic, Carrots, Bell Pepper, Potatoes, Chili, Flour, Liver Spread,Tomato Paste, Butter, Stock.' , 
    sub:'Caldereta name is derived from the Spanish word caldera meaning cauldron.',
    img:'/assets/images/main/caldereta.jpg',},

    { id: 2, title:'Chicken Inasal', descrip: 'The "chicken inasal" is a local version of the Filipino barbecue that originated from Bacolod City in the Visayas islands south of the Philippines. It is so popular in Bacolod City that street stands are selling chicken barbecue in almost every corner.',
    ingre: ' Main Ingredients: Chicken, Lime, Pepper, Vinegar, Anatto.' , 
    sub:'Commonly known simply as inasal, is a variant of Lechon manok.',
    img:'/assets/images/main/inasal.jpg',},

    { id: 2, title:'Menudo', descrip: 'The Filipino style of Menudo originated during the time of Spanish colonization in the Philippines. Furthermore, Sautéing is a method of cooking introduced by the Spaniards during their colony and is now implemented as the traditional culinary method in cooking the Filipino dish. The pork meat is first sautéed in garlic and onion before stewing in tomato sauce.',
    ingre: ' Main Ingredients: Pork, Liver (pork or beef), Carrots, Potatoes, Tomato Sauce.' , 
    sub:'Also known as ginamay is a traditional stew from the Philippines made with pork and sliced liver in tomato sauce with carrots and potatoes.',
    img:'/assets/images/main/menudo.jpg',},

    { id: 2, title:'Sinigang', descrip: 'Sinigang is often made for special occasions such as a birthday or a baptism but today it is categorized as a common dish in the Filipino culture. The dish is often assumed to be the national food of the Philippines in the sense of how common and how long it has been around and the flavoring of this dish depends on what practice is done in different regions.',
    ingre: ' Main Ingredients: Meat, Vegetables, Tamarind, Fish Sauce, Onions, Siling Mahaba, Tomatoes.' , 
    sub:'Sinigang is a Philippine dish that usually consists of fish, meat, vegetables, tomatoes, and tamarind flavoring.',
    img:'/assets/images/main/sinigang.png',},

    { id: 2, title:'Sisig', descrip: 'The existence of the dish was first recorded in a Kapampangan dictionary back in 1732 by Diego Bergaño, a Spanish missionary who served as the parish priest for Mexico, Pampanga at the time. The Augustinian friar defined sisig as “a salad including green papaya or green guava is eaten with a dressing of salt, pepper, garlic, and vinegar.”',
    ingre: ' Main Ingredients: Pork Jowls, Ears and Liver, Onions, Chili' , 
    sub:'The dish’s name comes from “sisigan,” an old Tagalog word that means “to make it sour.”',
    img:'/assets/images/main/sisig.jpg',},

    { id: 2, title:'Tinola', descrip: 'Tinola is said to be invented in the late 1800s, around two hundred years ago. But some believe the recipe was developed way earlier. Regardless of its origin, it has evolved into a dish that Filipinos can proudly call their own. The Tinolang Manok is a signature Filipino main dish that stood through the test of time—a chicken dish they still love and enjoy today.',
    ingre: ' Main Ingredients: Chicken, Green Papaya, Siling Labuyo Leaves, Ginger, Onion, Fish Sauce' , 
    sub:'Cooked with chicken or fish, wedges of green papaya, and leaves of the Siling Labuyo chili pepper in broth flavored with ginger, onions and fish sauce.',
    img:'/assets/images/main/tinola.jpg',},
  ];


// productId;

// foods: any = {};

//   constructor(private router: ActivatedRoute) {
// this.productId =this.router.snapshot.paramMap.get('id');
// this.router.queryParams.subscribe( res => {
//   console.log(res);
//   this.foods = res;
// })
//   }

  constructor(private router: Router) {}

goToUlam(ulams){

  this.router.navigate(['/ulam'],{
    queryParams: ulams 
  })
}

  ngOnInit() {
    // With Routing in Ionic, The OnInit lifecycle hook 
    // may not get called consistently.
    console.log("CategoriesPage - OnInit")
  }
  
  ngOnDestroy() {
    // Likewise, this will may not consistently fire when you navigate away
    // from the component
    console.log("CategoriesPage - OnDestroy")
  }


}
