import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-side',
  templateUrl: './side.page.html',
  styleUrls: ['./side.page.scss'],
})
export class SidePage implements OnInit  {

  side: any[] = [
    { id: 1, title:'Atsara', descrip: 'Atsara was influenced from India, with the dish Achaar ( a dish with varieties of fruits and vegetables) which means “pickle”. The Pickling process was used as a solution to preserve excess food for seasons when there was a lack of food production.     ',
    ingre: ' Main Ingredients: Unripe Papaya.' , 
    sub:'Atsara is usually found in many Filipino households to use as a side dish or palate cleanser for fried, grilled, and roasted dishes.',
    img: '/assets/images/main/atsara.jpg'},

    { id: 2, title:'Fried Rice', descrip: 'As a homemade dish, fried rice is typically made with ingredients leftover from other dishes, leading to countless variations. Moreover, the Fried rice was first developed during the Sui Dynasty in China, and as such all fried rice dishes can trace their origins to Chinese fried rice.',
    ingre: ' Main Ingredients: Cooked Rice, Cooking Oil' , 
    sub:'It is a popular side to almost all Filipino breakfast dishes.',
    img:'/assets/images/main/fried-rice.jpg',},

    { id: 2, title:'Ginisang Togue', descrip: 'Ginisang togue at sitsaro is a delicious Filipino dish of sauteed mung beans and snow peas. In Chinese cuisine, whole mung beans are used to make a tángshuǐ, or dessert, otherwise literally translated ‘sugar water’, called lǜdòu tángshuǐ, which is served either warm or chilled.',
     ingre: ' Main Ingredients: Mung Beans Sprout, Bell Pepper, Carrots, Shrimp, Tofu.' , 
     sub:'in Tagalog, ginisa means saute, togue means mung bean sprouts, and sitsaro means snow peas',
    img:'/assets/images/main/ginisang-togue.jpg',},

    { id: 2, title:'Kilawin', descrip: 'Kinilaw is native to the Philippines. The Balangay archaeological excavation site in Butuan (dated c. 10th to 13th century AD) has uncovered remains of halved tabon-tabon fruits and fish bones cut in a manner suggesting that they were cubed, thus indicating that the cooking process is at least a thousand years old.',
    ingre: ' Main Ingredients: Seafood/Meat/Vegetables, Vinegar, Calamansi, Onion, Ginger, Salt, Siling labuyo, Black pepper' , 
    sub:'It is also referred to as Philippine ceviche due to its similarity to the Latin American dish Ceviche',
    img:'/assets/images/main/kilawin.jpg',},

    { id: 2, title:'Latik', descrip: 'Latik refers to two different coconut-based ingredients in Filipino cuisine. In the Visayan region, it refers to a syrupy caramelized coconut cream (coconut caramel) used as a dessert sauce. In the northern Philippines, it refers to solid byproducts of coconut oil production (coconut curds), used as garnishing for a variety of desserts.',
    ingre: ' Main Ingredients: Curd of Coconut Milk.' , 
    sub:'Latík in its original sense in the Visayan languages means "syrup".',
    img:'/assets/images/main/latik.jpg',},

    { id: 2, title:'Lumpia', descrip: 'The origins date back centuries, as the Chinese originally sought to create a meal that incorporated all the fresh vegetables available in spring after a winter of consuming mostly preserved foods. It is believed that Chinese immigrants from the Fujian province brought this dish to Southeast Asia and that its popularity spread from there.',
    ingre: ' Main Ingredients: Wrapper, Meat, Vegetables.' , 
    sub:'The lumpia is derived from the Chinese spring roll.',
    img:'/assets/images/main/lumpia.jpg',},

    { id: 2, title:'Pan de sal', descrip: 'It is the Spanish term for “salt bread,” since the name originated during the 16th-century Spanish colonial era. Most bakeries all over the country, from small backyard establishments to industrial bakeries, produce and sell this bread.',
    ingre: ' Main Ingredients: Flour, Eggs, Yeast, Sugar, Salt, Milk.' , 
    sub:'Pandesal is the quintessential bread roll of the Philippines.',
    img:'/assets/images/main/pandesal.jpg',},

    { id: 2, title:'Puto', descrip: 'Puto is one of the staples in a classic Filipino gathering, whether that be a birthday party, fiesta or graduation it is very rare for a Filipino celebration to not have Puto as part of it. It is usually round-shaped with either cheese or salted egg toppings.',
    ingre: ' Main Ingredients: Rice.' , 
    sub:'It’s a light snack with a hint of sweetness in every bite kicked by the saltiness of the cheese and or the salted egg.',
    img:'/assets/images/main/puto.jpg',},

    { id: 2, title:'Salad', descrip: 'The Romans, ancient Greeks, and Persians ate mixed greens with dressing, a type of mixed salad. Salads, including layered and dressed salads, have been popular in Europe since the Greek and Roman imperial expansions',
    ingre: ' Main Ingredients: Pieces of vegetables, fruits, meat, eggs, or grains mixed with a sauce' , 
    sub:'A dish consisting of mixed pieces of food, typically with at least one raw ingredient.',
    img:'/assets/images/main/salad.jpg',},

    { id: 2, title:"Tokwa't Baboy", descrip: 'The Tofu ingredient originated in China during the Han Dynasty then due to Buddhism it spread fast in South East Asia because this is one of the major protein sources for the vegetarian diet. A very healthy alternative to protein which contains almost no fat, low in calories but high in iron.',
    ingre: ' Main Ingredients: Pork Ears, Pork Belly, Tofu, For Dip: Soy  Sauce, Pork Broth, Vinegar, White Onions, Scallions, Red Chili Peppers.' , 
    sub:'It is usually served as pulutan, as a meal served with rice, or as a side dish to rice porridge.',
    img:'/assets/images/main/tokwa.jpg',},
  ];


  constructor(private router: Router) {}

  goToSide(side){

    this.router.navigate(['/sidedish'],{
      queryParams: side 
    })
  }

  ngOnInit() {
    // With Routing in Ionic, The OnInit lifecycle hook 
    // may not get called consistently.
    console.log("CategoriesPage - OnInit")
  }
  
  ngOnDestroy() {
    // Likewise, this will may not consistently fire when you navigate away
    // from the component
    console.log("CategoriesPage - OnDestroy")
  }


}