import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BookSdishPageRoutingModule } from './book-sdish-routing.module';

import { BookSdishPage } from './book-sdish.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BookSdishPageRoutingModule
  ],
  declarations: [BookSdishPage]
})
export class BookSdishPageModule {}
