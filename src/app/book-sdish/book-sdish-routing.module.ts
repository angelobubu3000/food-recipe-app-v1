import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BookSdishPage } from './book-sdish.page';

const routes: Routes = [
  {
    path: '',
    component: BookSdishPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BookSdishPageRoutingModule {}
